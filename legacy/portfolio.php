<?php
  session_start();
?>

<!DOCTYPE html>
<html>
  <head>

  <title>Jake Butterfield</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
  
  <!-- Fonts -->
  	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,100" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Sarala" rel="stylesheet" type="text/css">
    
  <!-- Stylesheets -->
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="lightbox.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    
  <!-- Scripts -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  </head>
  
  <body>
  
  	<div class="header" style="background-color: #117bff;">
  		<div class="container">
  			<h1><span style="background-color: rgba(0, 0, 0, 0.4);">UNDER CONSTRUCTION</span></h1>
  			<p><span style="background-color: rgba(0, 0, 0, 0.4);">Check Back Later</span></p>
            <p><b></b></p><br>
  		</div>
  	</div>

  <?php
    include('nav.php')
  ?>

<div id="wrap">

  <div class="jumbotron" style="background-color: #117bff;">

    <div class="container">

      <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            
            <div class="thumbnail">
    		    <div class="caption">
    		        <img src="images/portfolio/mountains.jpg" width="90%">
        			<h3>Great Barrier Reef</h3><!-- style="color: lime" when complete-->
        			<p>I would love to dive or snorkel in this amazing place and see all the beautiful sights from down below.</p>
    		    </div>
		    </div>
            
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            
            <div class="thumbnail">
    		    <div class="caption">
    		        <img src="images/portfolio/mountains.jpg" width="90%">
        			<h3>Great Barrier Reef</h3><!-- style="color: lime" when complete-->
        			<p>I would love to dive or snorkel in this amazing place and see all the beautiful sights from down below.</p>
    		    </div>
		    </div>            
            
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            
            <div class="thumbnail">
    		    <div class="caption">
    		        <img src="images/portfolio/mountains.jpg" width="90%">
        			<h3>Great Barrier Reef</h3><!-- style="color: lime" when complete-->
        			<p>I would love to dive or snorkel in this amazing place and see all the beautiful sights from down below.</p>
    		    </div>
		    </div>            
            
        </div>

      </div>

    </div>

  </div>
        
</div> <!-- End of wrap -->
		
    <div id="footer">
      <div class="container footer" style="text-align: center;">
        <p class="text-muted">Website Designed & Developed by <a href="about.php">Jake Butterfield</a></p>
      </div>
    </div>
	
  </body>
</html>