<?php
  session_start();
?>

<!DOCTYPE html>
<html>
  <head>

  <title>Jake Butterfield</title>

  <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
  
  <!-- Fonts -->
  	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,100" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Sarala" rel="stylesheet" type="text/css">
    
  <!-- Stylesheets -->
	<link rel="stylesheet" href="./style.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    
  </head>
  
  <body>
  
  	<div class="header" style="background: url(images/travel.jpg) no-repeat center center; background-size: cover;">
  		<div class="container">
  			<h1><span style="background-color: rgba(0, 0, 0, 0.4);">Jake Butterfield</span></h1>
  			<p><span style="background-color: rgba(0, 0, 0, 0.4);">"If we were meant to stay in one place, we'd have roots instead of feet"</span></p>
            <p><b><span style="background-color: rgba(0, 0, 0, 0.4);">Rachel Wolchin</span></b></p>
  		</div>
  	</div>

  <?php
    include('nav.php')
  ?>

<div id="wrap">

    <div class="jumbotron" style="background-color: #117bff; color: white;">
      <h2><b>My Travel Diary</b></h2>
      <p>Travelling is something I love to do</p>
      <p>This is a sort of scrapbook with multiple images from my trips around the world</p>
      <p>This is also just some of the places I have been, a lot of my early travels have no images</p>
    </div>

    <div class="jumbotron" style="background-color: #fff; color: black; padding-left: 5%; padding-right: 5%;">
      <div class="row">
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/mexico.png" alt="Mexico" style="width:100%">
            <div class="caption">
              <p>Puerto Vallarta, Mexico</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/miami.png" alt="Miami" style="width:100%">
            <div class="caption">
              <p>Miami, Florida</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/florida.png" alt="Florida" style="width:100%">
            <div class="caption">
              <p>Orlando, Florida</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="jumbotron" style="background-color: #117bff; color: white; padding-left: 5%; padding-right: 5%;">
      <div class="row">
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/columbia.png" alt="Columbia" style="width:100%">
            <div class="caption">
              <p>Cartagena, Columbia</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/edinburgh.png" alt="Edinburgh" style="width:100%">
            <div class="caption">
              <p>Edinburgh, Scotland</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/dubai.png" alt="Dubai" style="width:100%">
            <div class="caption">
              <p>Dubai, UAE</p>
            </div>
          </div>
        </div>
      </div>
    </div>  

    <div class="jumbotron" style="background-color: #fff; color: black; padding-left: 5%; padding-right: 5%;">
      <div class="row">
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/tanzania.png" alt="Tanzania" style="width:100%">
            <div class="caption">
              <p>Tanzania, Africa</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/guatamala.png" alt="Guatemala" style="width:100%">
            <div class="caption">
              <p>Puerto Quetzal, Guatemala</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/lakedistrict.png" alt="Lake District" style="width:100%">
            <div class="caption">
              <p>Lake District, UK</p>
            </div>
          </div>
        </div>
      </div>
    </div>   

    <div class="jumbotron" style="background-color: #117bff; color: white; padding-left: 5%; padding-right: 5%;"">
      <div class="row">
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/costarica.png" alt="Costa Rica" style="width:100%">
            <div class="caption">
              <p>Puntarenas, Costa Rica</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/panama.png" alt="Panama" style="width:100%">
            <div class="caption">
              <p>Colon, Panama</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/newyork.png" alt="New York" style="width:100%">
            <div class="caption">
              <p>New York, New York</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="jumbotron" style="background-color: #fff; color: black; padding-left: 5%; padding-right: 5%;"">
      <div class="row">
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/la.jpg" alt="Los Angeles" style="width:100%">
            <div class="caption">
              <p>Los Angeles, California</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/sanfran.jpg" alt="San Francisco" style="width:100%">
            <div class="caption">
              <p>San Francisco, California</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="thumbnail" style="margin: 0;">
            <img src="images/travel/lasvegas.jpg" alt="Las Vegas" style="width:100%">
            <div class="caption">
              <p>Las Vegas, Nevada</p>
            </div>
          </div>
        </div>
      </div>
    </div>               

</div> <!-- End of wrap -->
		
    <div id="footer">
      <div class="container footer" style="text-align: center;">
        <p class="text-muted">Website Designed & Developed by <a href="about.php">Jake Butterfield</a></p>
      </div>
    </div>
	
	<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	
  </body>
</html>