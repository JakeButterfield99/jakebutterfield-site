<?php
  session_start();
?>

<!DOCTYPE html>
<html>
  <head>

  <title>Jake Butterfield</title>

  <link rel='shortcut icon' type='image/x-icon' href='images/favicon.png' />
  
  <!-- Fonts -->
  	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,100" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Sarala" rel="stylesheet" type="text/css">
    
  <!-- Stylesheets -->
	<link rel="stylesheet" href="./style.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    
  </head>
  
  <body>
  
  	<div class="header" style="background: url(images/info.jpg) no-repeat center center; background-size: cover;">
  		<div class="container">
  			<h1><span style="background-color: rgba(0, 0, 0, 0.4);">Jake Butterfield</span></h1>
  			<p><span style="background-color: rgba(0, 0, 0, 0.4);">"Whatever you are, be a good one"</span></p>
            <p><b><span style="background-color: rgba(0, 0, 0, 0.4);">Abraham Lincoln</span></b></p>
  		</div>
  	</div>

  <?php
    include('nav.php')
  ?>

<div id="wrap">

        <div class="jumbotron" style="background-color: #117bff; color: white;">
        	<div class="white">
	            <h2><b>Technical</b></h2>
	            <p>Made using <a href="http://getbootstrap.com/">Bootstrap</a></p>
	            <p>Hosted with <a href="https://perspective.host/">Perspective</a></p>
	            <p>Coded in HTML, CSS, JavaScript and PHP</p>
	        </div>
        </div>
		
		<div class="jumbotron" style="background-color: #fff;">      
	            <h2><b>Design</b></h2>
	            <p>Main 'block' design uses <a href="https://v4-alpha.getbootstrap.com/components/jumbotron/">Bootstrap Jumbotrons</a></p>
	            <p>All pages have a header which contains one of my favourite quotes, along with their author</p>
	            <p>This header also contains some of the best images from my travels abroad</p>   
		</div>

        <div class="jumbotron" style="background-color: #117bff; color: white;">
            <h2><b>Dates</b></h2>
            <p>Work on this site started in June 2016</p>
            <p>This site was upgraded to include PHP Elements in March 2017</p>
            <p>Site was last updated in October 2017</p>
        </div>

</div> <!-- End of wrap -->
		
    <div id="footer">
      <div class="container footer" style="text-align: center;">
        <p class="text-muted">Website Designed & Developed by <a href="about.php">Jake Butterfield</a></p>
      </div>
    </div>
	
	<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	
  </body>
</html>