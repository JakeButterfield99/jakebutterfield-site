<div class='nav'>
	<div class='container'>
       	<div class='fixed-nav'>
               <ul>
               	    <?php
						$url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
					?>

                   	<a href='home.php'><li <?php if (strpos($url, 'home') !== false) { ?>id='active'<?php } ?>>Home</li></a>
                   	<a href='about.php'><li <?php if (strpos($url, 'about') !== false) { ?>id='active'<?php } ?>>About</li></a>
                   	<a href='portfolio.php'><li <?php if (strpos($url, 'portfolio') !== false) { ?>id='active'<?php } ?>>Portfolio</li></a>
                    <a href='travel.php'><li <?php if (strpos($url, 'travel') !== false) { ?>id='active'<?php } ?>>Travel Diary</li></a>
                   	<a href='bucket-list.php'><li <?php if (strpos($url, 'bucket-list') !== false) { ?>id='active'<?php } ?>>Bucket List</li></a>
                   	<a href='contact.php'><li <?php if (strpos($url, 'contact') !== false) { ?>id='active'<?php } ?>>Contact</li></a>
                   	<a href='info.php'><li <?php if (strpos($url, 'info') !== false) { ?>id='active'<?php } ?>>Info</li></a>
	                   
               </ul>
        </div>
  	</div>
</div>